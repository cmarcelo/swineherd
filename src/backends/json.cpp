/* Copyright © 2018 Intel Corproation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <string>
#include <fstream>
#include <iostream>
#include <cstring>
#include <json/json.h>
#include <sys/types.h>
#include <dirent.h>
#include <unistd.h>
#include <errno.h>

#include "json.hpp"
#include "utils.hpp"
#include "zstr.hpp"

namespace swineherd {

namespace backend {

namespace {

Json::Value
toJson(RunTime & time) {
    Json::Value val{Json::objectValue};
    val["start"] = time.start;
    val["end"] = time.end;
    val["__type__"] = "TimeAttribute";
    return val;
}

Json::Value
toJson(Result & result) {
    Json::Value val{Json::objectValue};
    val["err"] = result.err;
    val["out"] = result.out;
    val["returncode"] = result.returncode;
    val["result"] = to_string(result.getStatus());
    val["__type__"] = "TestResult";  // required for piglit compatability
    val["command"] = utils::joinStrings(result.command.begin(), result.command.end(), " ");

    Json::Value st{Json::objectValue};
    for (auto const& kv : result.status) {
        if (kv.first == "") {
            continue;
        }
        st[kv.first] = to_string(kv.second);
    }
    val["subtests"] = st;

    val["time"] = toJson(result.time);

    return val;
}

Json::Value
toJson(const Options & opts) {
    Json::Value val{Json::objectValue};
    val["name"] = opts.name;

    Json::Value options{Json::objectValue};
    options["platform"] = opts.platform;
    options["jobs"] = opts.threads;

    Json::Value fil{Json::arrayValue};
    for (auto const & f : opts.include_filters) {
        fil.append(f.str);
    }
    options["include_filters"] = fil;

    Json::Value exf{Json::arrayValue};
    for (auto const & f : opts.exclude_filters) {
        exf.append(f.str);
    }
    options["exclude_filters"] = exf;

    val["options"] = options;
    val["results_version"] = 10;

    return val;
}

}

void
JsonBackend::writeTest(Group & testname, Result & result) {
    // XXX: is the memory_order correct?
    unsigned number = counter.fetch_add(1, std::memory_order_relaxed);
    std::string name{outdir};
    name.append("/tests/");
    name.append(std::to_string(number));
    name.append(".json");
    
    auto inner = toJson(result);
    Json::Value val{Json::objectValue};
    val[std::string(testname)] = inner;

    std::ofstream file;
    file.open(name);
    file << val;
    file.flush();
    file.close();
}

void
JsonBackend::finalize(RunTime & time) {
    // FIXME: with streams it should be possible to do this without building an
    // entire json object in memory, but for now this is easy and straightforward
    Json::Value root{Json::objectValue};
    root["results_version"] = 10;
    root["time_elapsed"] = toJson(time);
    root["__type__"] = "TestrunResult";

    Json::Value info{Json::objectValue};
    info["system"] = Json::Value{Json::objectValue};
    for (auto const & kv : environment) {
        info["system"][kv.first] = kv.second;
    }
    info["runner"] = "swineherd";

    root["info"] = info;

    Json::CharReaderBuilder rbuilder;
    Json::Value tests{Json::objectValue};

    DIR * dir = opendir((outdir + "/tests").c_str());
    struct dirent * ent;
    std::string errs;
    while ((ent = readdir(dir)) != NULL) {
        std::string fname{ent->d_name};
        if (fname[0] == '.') {
            continue;
        }
        std::ifstream file;
        file.open(outdir + "/tests/" + fname);

        Json::Value test;
        bool ok = Json::parseFromStream(rbuilder, file, &test, &errs);
        if (!ok) {
            abort();
        }

        std::string tname = test.getMemberNames()[0];
        tests[tname] = test[tname];
    }
    closedir(dir);

    root["tests"] = tests;

    std::ifstream file;
    file.open(outdir + "/metadata.json");
    Json::Value meta;
    bool ok = Json::parseFromStream(rbuilder, file, &meta, &errs);
    if (!ok) {
        abort();
    }
    file.close();
    (void)unlink((outdir + "/metadata.json").c_str());

    root["name"] = meta["name"];
    root["options"] = meta["options"];

    std::string oname{outdir + "/results.json.gz"};
    zstr::ofstream ofile{oname};
    ofile << root;
    ofile.flush();

    dir = opendir((outdir + "/tests").c_str());
    while ((ent = readdir(dir)) != NULL) {
        std::string fname{ent->d_name};
        if (fname[0] == '.') {
            continue;
        }
        (void)unlink((outdir + "/tests/" + fname).c_str());
    }
    closedir(dir);
    (void)rmdir((outdir + "/tests/").c_str());
}

void
JsonBackend::initialize(const Options & opts) {
    try {
        utils::makedir(outdir);
    } catch (utils::MkdirError & e) {
        if (e.err != EEXIST) {
            throw;
        }
    }

    Json::Value meta = toJson(opts);
    std::string oname{outdir + "/metadata.json"};
    std::ofstream ofile;
    ofile.open(oname);
    ofile << meta;
    ofile.flush();
    ofile.close();

    try {
        utils::makedir(outdir + "/tests");
    } catch (utils::MkdirError & e) {
        if (e.err != EEXIST) {
            throw;
        }
    }
}

}

}
